using System;
using System.Collections.Generic;
using System.Linq;
using Vintagestory.API.Common;
using Vintagestory.API.Common.Entities;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.GameContent;


namespace WaypointSync
{
    public static class Utils
    {
        public const string waypointJSON = "waypointgroups.json";

        public static List<Waypoint> GetAllWaypoints(ICoreServerAPI ServerAPI)
        {
            var worldMapManager = ServerAPI.ModLoader.GetModSystem<WorldMapManager>();
            var maplayers = worldMapManager.MapLayers;
            var waypointMapLayer = (WaypointMapLayer)maplayers.Where(m => typeof(WaypointMapLayer) == m.GetType()).Single();
            return waypointMapLayer.Waypoints;
        }

        public static List<WpGroup> LoadWpGroups(ICoreServerAPI ServerAPI)
        {
            try
            {
                return ServerAPI.LoadModConfig<List<WpGroup>>(waypointJSON);
            }
            catch (System.Exception)
            {
                ServerAPI.Logger.Error("Invalid waypoint groups JSON");
                return null;
            }
        }
        public static int StringToColorInt(string colorString, IServerPlayer player)
        {
            System.Drawing.Color val;
            if (colorString.StartsWith("#"))
            {
                try
                {
                    val = System.Drawing.Color.FromArgb(int.Parse(colorString.Replace("#", ""), System.Globalization.NumberStyles.HexNumber));
                }
                catch (FormatException)
                {
                    player.SendMessage(GlobalConstants.GeneralChatGroup, Lang.Get("command-waypoint-invalidcolor"), EnumChatType.CommandError);
                    val = System.Drawing.Color.FromName(WpGroup.GetDefaultGroup().Color);
                }
            }
            else
            {
                val = System.Drawing.Color.FromName(colorString);
            }
            return (val.ToArgb() | -16777216);

        }

        public static bool PositionEqual(Vec3d p1, Vec3d p2)
        {
            return (p1 - p2).Length() < 1;
        }

    }
    public class FakePlayer : IServerPlayer
    {
        public FakePlayer(string PlayerUID)
        {
            this.PlayerUID = PlayerUID;
        }
        public EnumClientState ConnectionState => EnumClientState.Offline;
        public string PlayerUID { get; private set; }
        public int ItemCollectMode { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }
        public int CurrentChunkSentRadius { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public string IpAddress => throw new NotImplementedException();

        public string LanguageCode => throw new NotImplementedException();

        public float Ping => throw new NotImplementedException();

        public IServerPlayerData ServerData => throw new NotImplementedException();

        public IPlayerRole Role { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public PlayerGroupMembership[] Groups => throw new NotImplementedException();

        public FuzzyEntityPos SpawnPosition => throw new NotImplementedException();

        public List<Entitlement> Entitlements => throw new NotImplementedException();

        public BlockSelection CurrentBlockSelection => throw new NotImplementedException();

        public EntitySelection CurrentEntitySelection => throw new NotImplementedException();

        public string PlayerName => throw new NotImplementedException();

        public int ClientId => throw new NotImplementedException();

        public EntityPlayer Entity => throw new NotImplementedException();

        public IWorldPlayerData WorldData => throw new NotImplementedException();

        public IPlayerInventoryManager InventoryManager => throw new NotImplementedException();

        public string[] Privileges => throw new NotImplementedException();

        public bool ImmersiveFpMode => throw new NotImplementedException();

        public void BroadcastPlayerData(bool sendInventory = false)
        {
            throw new NotImplementedException();
        }

        public void ClearSpawnPosition()
        {
            throw new NotImplementedException();
        }

        public void Disconnect()
        {
            throw new NotImplementedException();
        }

        public void Disconnect(string message)
        {
            throw new NotImplementedException();
        }

        public PlayerGroupMembership GetGroup(int groupId)
        {
            throw new NotImplementedException();
        }

        public PlayerGroupMembership[] GetGroups()
        {
            throw new NotImplementedException();
        }

        public byte[] GetModdata(string key)
        {
            throw new NotImplementedException();
        }

        public bool HasPrivilege(string privilegeCode)
        {
            throw new NotImplementedException();
        }

        public void RemoveModdata(string key)
        {
            throw new NotImplementedException();
        }

        public void SendIngameError(string code, string message = null, params object[] langparams)
        {
            throw new NotImplementedException();
        }

        public void SendMessage(int groupId, string message, EnumChatType chatType, string data = null)
        {
            throw new NotImplementedException();
        }

        public void SendPositionToClient()
        {
            throw new NotImplementedException();
        }

        public void SetModdata(string key, byte[] data)
        {
            throw new NotImplementedException();
        }

        public void SetRole(string roleCode)
        {
            throw new NotImplementedException();
        }

        public void SetSpawnPosition(PlayerSpawnPos pos)
        {
            throw new NotImplementedException();
        }
    }
}
