using System;
using System.Collections.Generic;
using System.Linq;
using Vintagestory.API.Common;
using Vintagestory.API.Config;
using Vintagestory.API.MathTools;
using Vintagestory.API.Server;
using Vintagestory.API.Util;
using Vintagestory.GameContent;

namespace WaypointSync
{
    public class WayPointDelegates
    {
        public const string swpSyntax = "Syntax: /swp [add|addat|pull|push|pushall|remove]";
        private const string addSyntax = "Syntax: /swp [add|addat pos] wp-group [name]";
        private const string pullSyntax = "Syntax: /swp pull [wp-group]";
        private const string pushSyntax = "Syntax: /swp push [name|id] [wp-group]";
        private const string pushallSyntax = "Syntax: /swp pushall [wp-group] name-start";
        public static ServerChatCommandDelegate SwpCommandDelegate(ICoreServerAPI api)
        {
            return delegate (IServerPlayer player, int _, CmdArgs args)
            {
                var subCmd = args.PopWord();
                switch (subCmd)
                {
                    case "addat":
                    {
                        var argPosition = args.PopFlexiblePos(player.Entity.Pos.XYZ, api.World.DefaultSpawnPosition.XYZ);
                        if (argPosition == null)
                        {
                            player.SendMessage(GlobalConstants.GeneralChatGroup, addSyntax, EnumChatType.CommandSuccess);
                            return;
                        }
                        CreateSyncedWaypoint(api, player, args, argPosition);
                        break;
                    }
                    case "add":
                    {
                        var argPosition = player.Entity.Pos.XYZ;
                        CreateSyncedWaypoint(api, player, args, argPosition);
                        break;
                    }
                    case "sync":
                    case "pull":
                        if (args.Length == 0)
                        {
                            player.SendMessage(GlobalConstants.GeneralChatGroup, pullSyntax, EnumChatType.CommandSuccess);
                            return;
                        }
                        SyncWaypointGroup(api, player, args);
                        break;
                    case "push":
                    {
                        if (args.Length == 0)
                        {
                            player.SendMessage(GlobalConstants.GeneralChatGroup, pushSyntax, EnumChatType.CommandSuccess);
                            return;
                        }
                        if (int.TryParse(args.PeekWord(""), out var result))
                        {
                            args.PopWord();
                            WpPushById(api, player, args, result);
                            return;
                        }
                        var argWpName = args.PopWord("");
                        var argGroup = args.PopWord(WpGroup.GetDefaultGroup().GroupName);
                        WpPushByName(api, player, args, argGroup, argWpName);
                        break;
                    }
                    case "pushall":
                    {
                        if (args.Length <= 1)
                        {
                            player.SendMessage(GlobalConstants.GeneralChatGroup, pushallSyntax, EnumChatType.CommandSuccess);
                            return;
                        }
                        var argGroup = args.PopWord(WpGroup.GetDefaultGroup().GroupName);
                        WpPushByName(api, player, args, argGroup);
                        break;
                    }
                    case "remove":
                        RemoveWaypointById(api, player, args);
                        break;
                    default:
                        player.SendMessage(GlobalConstants.GeneralChatGroup, $"Subcommand {subCmd} not recognized.", EnumChatType.CommandSuccess);
                        player.SendMessage(GlobalConstants.GeneralChatGroup, swpSyntax, EnumChatType.CommandSuccess);
                        break;
                }
            };
        }
        public static ServerChatCommandDelegate DebugRemoveWaypointByName(ICoreServerAPI api)
        {
            var ServerAPI = api;
            return delegate (IServerPlayer player, int _, CmdArgs args)
            {
                var waypoints = Utils.GetAllWaypoints(ServerAPI);

                if (args.Length < 2)
                {
                    player.SendMessage(GlobalConstants.GeneralChatGroup, $"Syntax: /remove-waypoint wp-group name", EnumChatType.CommandError);
                    return;
                }
                var group = args.PopWord(WpGroup.GetDefaultGroup().GroupName);
                var name = args.PopWord(WpGroup.GetDefaultGroup().WpName);
                var toRemove = waypoints.Where(w => w.Title == name && w.Text == group);
                waypoints.RemoveAll(w => toRemove.Contains(w));

                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Removed {toRemove.Count()} Waypoints.", EnumChatType.CommandSuccess);
            };
        }
        public static void RemoveWaypointById(ICoreServerAPI api, IServerPlayer player, CmdArgs args)
        {
            var waypoints = Utils.GetAllWaypoints(api);

            if (args.Length < 2)
            {
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Syntax: /swp remove group id", EnumChatType.CommandError);
                return;
            }
            var groups = WpGroup.AllGroups;
            var groupArg = args.PopWord(WpGroup.GetDefaultGroup().GroupName);
            var group = groups.FirstOrDefault(g => g.GroupName == groupArg);
            if (group == null)
            {
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Group {groupArg} not found!", EnumChatType.CommandError);
                return;
            }
            var id = args.PopInt() ?? -1;
            var foundWp = GetWpById(player, id, waypoints);
            if (foundWp == null)
            {
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Waypoint {id} not found!", EnumChatType.CommandError);
                return;
            }

            // remove from all players
            var toRemove = waypoints.Where(w => Utils.PositionEqual(w.Position, foundWp.Position)).ToList();
            waypoints.RemoveAll(w => toRemove.Contains(w));
            // remove from group
            group.Waypoints.RemoveAll(wp => Utils.PositionEqual(wp.Position, foundWp.Position));

            player.SendMessage(GlobalConstants.GeneralChatGroup, $"Removed {toRemove.Count()} Waypoints.", EnumChatType.CommandSuccess);
        }

        public static ServerChatCommandDelegate DebugListAllWaypoints(ICoreServerAPI api)
        {
            return delegate (IServerPlayer player, int _, CmdArgs args)
            {
                var waypoints = Utils.GetAllWaypoints(api); // TODO may we sometimes want only some waypoints?
                var count = 0;
                foreach (var wp in waypoints)
                {
                    player.SendMessage(GlobalConstants.GeneralChatGroup, $"{count}: {wp.Title} {wp.Text} {wp.Position} owner: {wp.OwningPlayerUid}",
                                        EnumChatType.CommandSuccess);
                    count++;
                }
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Found {count} Waypoints.", EnumChatType.CommandSuccess);
            };
        }

        public static ServerChatCommandDelegate DebugFixAllWaypoints(ICoreServerAPI api)
        {
            return delegate (IServerPlayer player, int _, CmdArgs args)
            {
                var waypoints = Utils.GetAllWaypoints(api);
                var origCount = waypoints.Count;
                var _res = new List<Waypoint>();
                var count = 0;
                foreach (var wp in waypoints)
                {
                    if (!(wp.Icon == null || wp.Position == null || wp.Title == null))
                    {
                        _res.Add(wp);
                    }
                    else
                    {
                        count++;
                    }
                }
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Removed {count}/{origCount} Waypoint.", EnumChatType.CommandSuccess);
                waypoints.Clear();
                waypoints.AddRange(_res);
                count = Utils.GetAllWaypoints(api).Count;
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"New Count: {count} Waypoints.", EnumChatType.CommandSuccess);
            };
        }
        public static ServerChatCommandDelegate BackupWaypoints(ICoreServerAPI api)
        {
            return delegate (IServerPlayer player, int _, CmdArgs args)
            {
                var waypoints = Utils.GetAllWaypoints(api);
                var count = waypoints.Count;
                var data = SerializerUtil.Serialize(waypoints);
                api.WorldManager.SaveGame.StoreData("waypointBackup", data);
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Backed up {count} Waypoints.", EnumChatType.CommandSuccess);
            };
        }
        public static ServerChatCommandDelegate RestoreWaypoints(ICoreServerAPI api)
        {
            return delegate (IServerPlayer player, int _, CmdArgs args)
            {
                var waypoints = Utils.GetAllWaypoints(api);

                var data = api.WorldManager.SaveGame.GetData("waypointBackup");
                var newWaypoints = data == null ? new List<Waypoint>() : SerializerUtil.Deserialize<List<Waypoint>>(data);
                if (newWaypoints.Count > 0)
                {
                    waypoints.Clear();
                    waypoints.AddRange(newWaypoints);
                }
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Restored {newWaypoints.Count} Waypoints.", EnumChatType.CommandSuccess);
            };
        }

        public static void CreateSyncedWaypoint(ICoreServerAPI api, IServerPlayer player, CmdArgs args, Vec3d pos)
        {
            var argWpGroup = args.PopWord();
            if (argWpGroup == null)
            {
                player.SendMessage(GlobalConstants.GeneralChatGroup, addSyntax, EnumChatType.CommandError);

            }
            var argName = args.PopWord(WpGroup.GetDefaultGroup().WpName);

            var wpgroups = WpGroup.AllGroups;

            WpGroup group;
            try
            {
                group = wpgroups.SingleOrDefault(g => g.GroupName == argWpGroup);
                if (group == null)
                {
                    player.SendMessage(GlobalConstants.GeneralChatGroup, $"Group '{argWpGroup}' not found!", EnumChatType.CommandError);
                    player.SendMessage(GlobalConstants.GeneralChatGroup, addSyntax, EnumChatType.CommandError);
                    return;
                }
            }
            catch (InvalidOperationException)
            {
                player.SendMessage(GlobalConstants.GeneralChatGroup,
                                    $"Multiple group definitions encounterend in waypointgroups.json ({argWpGroup})", EnumChatType.CommandSuccess);
                group = wpgroups.FirstOrDefault(g => g.GroupName == argWpGroup) ?? WpGroup.GetDefaultGroup();
            }


            string wpname = argName == WpGroup.GetDefaultGroup().WpName
                          ? group.NextWpName()
                          : argName;

            var icon = group.Icon;
            var color = Utils.StringToColorInt(group.Color, player);

            var wp = new Waypoint
            {
                Position = pos,
                Color = color,
                Icon = icon,
                Title = wpname,
                OwningPlayerUid = player.PlayerUID,
            };
            group.Waypoints.Add(wp);
            player.SendMessage(GlobalConstants.GeneralChatGroup, $"Created Waypoint {wpname} in group {argWpGroup} at {pos}!", EnumChatType.CommandSuccess);
            PushWp(api, group, wp);
        }

        public static void SyncWaypointGroup(ICoreServerAPI api, IServerPlayer player, CmdArgs args)
        {
            var waypoints = Utils.GetAllWaypoints(api);
            var groupArgs = args.PopAll();
            if (groupArgs == null || groupArgs == "")
            {
                groupArgs = WpGroup.GetDefaultGroup().GroupName;
            }
            var groupNames = groupArgs.Split(' ');

            var groups = WpGroup.AllGroups.Where(g => groupNames.Contains(g.GroupName)).ToList();

            var _result = new List<Waypoint>();


            var ownWaypoints = waypoints.Where(w => w.OwningPlayerUid == player.PlayerUID).ToList();
            var aCount = 0;
            var mCount = 0;
            groups.ForEach(g =>
            {
                g.Waypoints.Where(wp => wp.Position != null)
                                .ToList().ForEach(wp =>
                                {
                                    var samePosWp = ownWaypoints.FirstOrDefault(ow => Utils.PositionEqual(wp.Position, ow.Position));
                                    if (samePosWp == null)
                                    {
                                        aCount++;
                                        var newWp = new Waypoint
                                        {
                                            Color = wp.Color,
                                            Position = wp.Position,
                                            Icon = wp.Icon,
                                            Title = wp.Title,
                                            OwningPlayerUid = player.PlayerUID,
                                        };
                                        _result.Add(newWp);
                                        ownWaypoints.Add(newWp);
                                    }
                                    else if (samePosWp.Title != wp.Title ||
                                             samePosWp.Color != wp.Color ||
                                             samePosWp.Icon != wp.Icon)
                                    {
                                        mCount++;
                                        samePosWp.Title = wp.Title;
                                        samePosWp.Color = wp.Color;
                                        samePosWp.Icon = wp.Icon;
                                    }
                                });
            });

            waypoints.AddRange(_result);
            if (player.ConnectionState == EnumClientState.Playing)
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Added {aCount}, modified {mCount} Waypoints from group(s) {groupArgs}", EnumChatType.CommandSuccess);
        }
        public static void WpPushByName(ICoreServerAPI api, IServerPlayer player, CmdArgs args, string argGroup, string argWpName = null)
        {
            var waypoints = Utils.GetAllWaypoints(api);
            var knownGroups = WpGroup.AllGroups;
            var foundgroup = knownGroups.FirstOrDefault(g => g.GroupName == argGroup);
            if (foundgroup == null)
            {
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Group '{argGroup}' not found.", EnumChatType.CommandError);
                player.SendMessage(GlobalConstants.GeneralChatGroup, pushSyntax, EnumChatType.CommandError);
                return;
            }
            if (argWpName == string.Empty)
            {
                player.SendMessage(GlobalConstants.GeneralChatGroup, pushSyntax, EnumChatType.CommandError);
                return;
            }
            if (argWpName == null)
            {
                argWpName = args.PopWord("NODEFAULT");
                var foundWps = waypoints.Where(w => w.OwningPlayerUid == player.PlayerUID
                                                     && w.Title.StartsWith(argWpName)).ToList();
                if (foundWps.Count == 0)
                {
                    player.SendMessage(GlobalConstants.GeneralChatGroup, $"No waypoint named '{argWpName}' found.", EnumChatType.CommandError);
                    player.SendMessage(GlobalConstants.GeneralChatGroup, pushallSyntax, EnumChatType.CommandError);
                    return;
                }
                foundWps.ForEach(w =>
                {
                    PushWp(api, foundgroup, w);
                });
            }
            else
            {
                var foundWp = waypoints.FirstOrDefault(w => w.OwningPlayerUid == player.PlayerUID
                                                        && w.Title == argWpName);
                if (foundWp == null)
                {
                    player.SendMessage(GlobalConstants.GeneralChatGroup, $"No waypoint named '{argWpName}' found.", EnumChatType.CommandError);
                    player.SendMessage(GlobalConstants.GeneralChatGroup, pushSyntax, EnumChatType.CommandError);
                    return;
                }
                PushWp(api, foundgroup, foundWp);
            }
        }
        public static void WpPushById(ICoreServerAPI api, IServerPlayer player, CmdArgs args, int id)
        {
            var waypoints = Utils.GetAllWaypoints(api);
            var knownGroups = WpGroup.AllGroups;
            var argGroup = args.PopWord(WpGroup.GetDefaultGroup().GroupName);
            var foundgroup = knownGroups.FirstOrDefault(g => g.GroupName == argGroup) ?? WpGroup.GetDefaultGroup();
            var foundWp = GetWpById(player, id, waypoints);
            if (foundWp != null)
                PushWp(api, foundgroup, foundWp);
        }

        private static Waypoint GetWpById(IServerPlayer player, int id, List<Waypoint> waypoints)
        {
            try
            {
                return waypoints.Where(w => w.OwningPlayerUid == player.PlayerUID)
                                       .ElementAt(id);

            }
            catch (ArgumentOutOfRangeException)
            {
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Could not find waypoint with given ID {id}", EnumChatType.CommandError);
                return null;
            }
        }

        private static void PushWp(ICoreServerAPI api, WpGroup foundgroup, Waypoint foundWp)
        {
            var waypoint = foundgroup.Waypoints.FirstOrDefault(wp => Utils.PositionEqual(foundWp.Position, wp.Position));
            if (waypoint == null)
            {
                foundgroup.Waypoints.Add(foundWp);
            }
            else
            {
                waypoint.Title = foundWp.Title;
                waypoint.Icon = foundWp.Icon;
                waypoint.Color = foundWp.Color;
            }
            var subscriberIds = foundgroup.SubscribedPlayerIds;
            subscriberIds.ForEach(s =>
            {
                IServerPlayer sub = api.World.PlayerByUid(s) as IServerPlayer ?? new FakePlayer(s);
                SyncWaypointGroup(api, sub, new CmdArgs(foundgroup.GroupName));
            });
        }
    }
}