﻿using System.Collections.Generic;
using Vintagestory.API.Client;
using Vintagestory.API.Common;
using Vintagestory.API.Server;
using Vintagestory.API.Util;

[assembly: ModInfo("WaypointSync",
    Description = "Synced waypoints for multiplayer Vintage Story",
    Website = "",
    Version = "0.9.0-rc1",
    Side = "Server",
    Authors = new[] { "aamuuninen", "BierLiebHaber" })]

namespace WaypointSync
{
    public class WaypointSync : ModSystem
    {
        // Server
        public ICoreServerAPI ServerAPI { get; private set; }

        public override bool ShouldLoad(EnumAppSide forSide)
        {
            return forSide == EnumAppSide.Server;
        }

        public override void Start(ICoreAPI api)
        {
        }

        public override void StartServerSide(ICoreServerAPI api)
        {
            ServerAPI = api;

            // TODO add commands: modify wp group
            // TODO add priviliges for remove methods

            ServerAPI.RegisterCommand("remove-waypoint", "Hard Remove a Synced Waypoint from the server", "wp-group name",
                                        WayPointDelegates.DebugRemoveWaypointByName(ServerAPI), Privilege.controlserver);
            ServerAPI.RegisterCommand("fix-waypoints", "Remove all broken waypoints from the server.", "",
                                        WayPointDelegates.DebugFixAllWaypoints(ServerAPI), Privilege.controlserver);
            ServerAPI.RegisterCommand("list-waypoints", "List all waypoints on the server.", "",
                                        WayPointDelegates.DebugListAllWaypoints(ServerAPI), Privilege.controlserver);
            ServerAPI.RegisterCommand("wpg-removeall", "Remove all groups", "wpId group",
                                        GroupDelegates.DebugRemoveAllGroups(), Privilege.controlserver);
            ServerAPI.RegisterCommand("wpg-fix", "Fix all groups", "",
                                        GroupDelegates.DebugFixAllGroups(), Privilege.controlserver);
            ServerAPI.RegisterCommand("wpg-list", "List all groups with debug info.", "",
                                        GroupDelegates.DebugListGroups(), Privilege.controlserver);

            ServerAPI.RegisterCommand("wpg-import", "Import waypoint groups from old json.", "",
                                        GroupDelegates.ImportOldGroups(ServerAPI), Privilege.controlserver);
            ServerAPI.RegisterCommand("swp-backup", "Backup all waypoints.", "",
                                        WayPointDelegates.BackupWaypoints(ServerAPI), Privilege.controlserver);
            ServerAPI.RegisterCommand("swp-restore", "Restore waypoints from backup.", "",
                                        WayPointDelegates.RestoreWaypoints(ServerAPI), Privilege.controlserver);

            ServerAPI.RegisterCommand("swp", "Manage Synced Waypoints", WayPointDelegates.swpSyntax,
                                        WayPointDelegates.SwpCommandDelegate(ServerAPI), Privilege.chat);

            ServerAPI.RegisterCommand("wpg", "Manage Synced waypoint groups.", "[add|list|sub|unsub|modify]",
                                        GroupDelegates.WpgCommandDelegate(ServerAPI), Privilege.chat);

            ServerAPI.Event.SaveGameLoaded += OnLoad;
            ServerAPI.Event.GameWorldSave += OnSave;
        }

        private void OnLoad()
        {
            byte[] data = ServerAPI.WorldManager.SaveGame.GetData("waypointGroups");
            try
            {
                WpGroup.AllGroups = (data == null || data.Length == 0) ? new List<WpGroup>() : JsonUtil.FromBytes<List<WpGroup>>(data);
            }
            catch (System.Exception e)
            {
                ServerAPI.World.Logger.Error("Cound not load Waypoint groups: ", e);
                WpGroup.AllGroups = new List<WpGroup>();
            }
        }

        private void OnSave()
        {
            byte[] data = JsonUtil.ToBytes(WpGroup.AllGroups);
            ServerAPI.WorldManager.SaveGame.StoreData("waypointGroups", data);
        }

        public override void StartClientSide(ICoreClientAPI api)
        {
        }




    }
}
