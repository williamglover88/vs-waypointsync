using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using ProtoBuf;
using Vintagestory.API.Common;
using Vintagestory.API.Config;
using Vintagestory.API.Server;
using Vintagestory.GameContent;

namespace WaypointSync
{
    [ProtoContract(ImplicitFields = ImplicitFields.AllPublic)]
    public class WpGroup
    {

        [JsonConstructor]
        public WpGroup(string groupName, string _wpname, string icon, string color, int count, List<string> subscribedPlayerIds, List<Waypoint> waypoints)
        {
            this.SubscribedPlayerIds = subscribedPlayerIds ?? new List<string>();
            this.GroupName = groupName;
            this.WpName = _wpname;
            this.Icon = icon;
            this.Color = color;
            this.Count = count;
            this.Waypoints = waypoints ?? new List<Waypoint>();
        }
        public string GroupName { get; }

        [JsonProperty]
        public string WpName { get; private set; }
        public string NextWpName()
        {
            Count += 1;
            return $"{WpName}-{Count}";
        }


        public string Icon { get; private set; }
        public string Color { get; }
        public List<string> SubscribedPlayerIds { get; set; }
        public int Count { get; private set; }

        public List<Waypoint> Waypoints;

        public override string ToString()
        {
            var subIdsString = "";
            SubscribedPlayerIds.ForEach(g => subIdsString += " " + g.ToString());
            return $"WPG: {GroupName} {WpName} {Icon} {Color} {Count} {subIdsString}";
        }

        public static WpGroup GetDefaultGroup(IServerPlayer player)
        {
            player.SendMessage(GlobalConstants.GeneralChatGroup, "Group not found, using default group!", EnumChatType.CommandError);

            return GetDefaultGroup();
        }

        public static WpGroup GetDefaultGroup()
        {
            return new WpGroup(groupName: "DEFAULTGROUP", _wpname: "DEFAULTWPNAME", icon: "circle", color: "black", count: 0, null, null);
        }

        public static List<WpGroup> AllGroups;

        public static readonly string[] Icons = {
            "circle",
            "bee",
            "cave",
            "home",
            "ladder",
            "pick",
            "rocks",
            "ruins",
            "spiral",
            "star1",
            "star2",
            "trader",
            "vessel"
        };


    }
}