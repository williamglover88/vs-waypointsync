using System;
using System.Collections.Generic;
using System.Linq;
using Vintagestory.API.Common;
using Vintagestory.API.Config;
using Vintagestory.API.Server;
using Vintagestory.GameContent;


namespace WaypointSync
{
    public static class GroupDelegates
    {
        public static ServerChatCommandDelegate WpgCommandDelegate(ICoreServerAPI serverAPI)
        {
            return delegate (IServerPlayer player, int _, CmdArgs args)
            {
                switch (args.PopWord())
                {
                    case "add":
                        if (!player.Privileges.Contains(Privilege.announce))
                        {
                            player.SendMessage(GlobalConstants.GeneralChatGroup, $"Insufficent privileges.", EnumChatType.CommandError);
                            return;
                        }
                        if (args.Length == 0)
                        {
                            player.SendMessage(GlobalConstants.GeneralChatGroup, $"Syntax: /wpg add group-name [default-name] [icon] [color]", EnumChatType.CommandError);
                            return;
                        }
                        AddWpGroup(player, args);
                        break;
                    case "list":
                        ListWpGroups(player);
                        break;

                    case "sub":
                    {
                        var argGroup = args.PeekWord();
                        var groups = WpGroup.AllGroups;
                        var group = groups.SingleOrDefault(g => g.GroupName == argGroup);
                        if (group == null)
                        {
                            player.SendMessage(GlobalConstants.GeneralChatGroup, $"Syntax: /wpg sub groupname", EnumChatType.CommandError);
                            return;
                        }

                        if (group.SubscribedPlayerIds.Contains(player.PlayerUID))
                        {
                            player.SendMessage(GlobalConstants.GeneralChatGroup, $"Already subscribed to {argGroup}", EnumChatType.CommandSuccess);
                        }
                        else
                        {
                            group.SubscribedPlayerIds.Add(player.PlayerUID);
                            player.SendMessage(GlobalConstants.GeneralChatGroup, $"Subscribed to group {argGroup}", EnumChatType.CommandSuccess);
                        }
                        WayPointDelegates.SyncWaypointGroup(serverAPI, player, args);

                        break;
                    }
                    case "unsub":
                        PurgeSubscription(serverAPI, player, args);
                        break;
                    case "modify":
                        ModifyGroup(serverAPI, player, args);
                        break;
                    default:
                        player.SendMessage(GlobalConstants.GeneralChatGroup, $"Syntax: /wpg [add|list|sub|unsub|modify]", EnumChatType.CommandError);
                        break;
                }
            };
        }

        private static void ModifyGroup(ICoreServerAPI serverAPI, IServerPlayer player, CmdArgs args)
        {
            var syntax = "Syntax: /wpg modify groupname [default-wpname] [icon] [color]";

            void SendError(string message)
            {
                player.SendMessage(GlobalConstants.GeneralChatGroup, message, EnumChatType.CommandError);
                player.SendMessage(GlobalConstants.GeneralChatGroup, syntax, EnumChatType.CommandError);
            };
            if (args.Length == 0)
            {
                SendError("");
                return;
            }
            if (args.Length > 4)
            {
                SendError("Too many arguments!");
                return;
            }
            var groupName = args.PopWord();
            if (groupName == null)
            {
                SendError("Groupname can not be null.");
                return;
            }
            var group = WpGroup.AllGroups.FirstOrDefault(g => g.GroupName == groupName);
            if (group == null)
            {
                SendError($"Group {groupName} not found.");
                return;
            }
            var wpName = args.PopWord(group.WpName);
            var icon = args.PopWord(group.Icon);
            if (!WpGroup.Icons.Any(i => i == icon))
            {
                SendError($"Icon {icon} not found.");
            }
            var color = args.PopWord(group.Color);
            var colorInt = Utils.StringToColorInt(color, player);

            group.Waypoints.ForEach(wp =>
            {
                wp.Icon = icon;
                wp.Color = colorInt;
                var titleList = wp.Title.Split('-');
                if (titleList.Length < 2)
                {
                    return;
                }
                if (int.TryParse(titleList[1], out var num))
                {
                    wp.Title = $"{wpName}-{num}";
                }
            });
            group.SubscribedPlayerIds.ForEach(s =>
            {
                IServerPlayer sub = serverAPI.World.PlayerByUid(s) as IServerPlayer ?? new FakePlayer(s);
                WayPointDelegates.SyncWaypointGroup(serverAPI, sub, new CmdArgs(groupName));
            });
        }

        public static ServerChatCommandDelegate DebugRemoveAllGroups()
        {
            return delegate (IServerPlayer player, int _, CmdArgs args)
            {
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Removed {WpGroup.AllGroups.Count} groups.", EnumChatType.CommandSuccess);
                var wpgroups = new List<WpGroup>();
            };
        }

        public static ServerChatCommandDelegate DebugListGroups()
        {
            return delegate (IServerPlayer player, int _, CmdArgs args)
            {
                WpGroup.AllGroups.ForEach(g =>
                {
                    player.SendMessage(GlobalConstants.GeneralChatGroup, $"{g}", EnumChatType.CommandSuccess);
                });
            };
        }
        public static ServerChatCommandDelegate ImportOldGroups(ICoreServerAPI serverAPI)
        {
            return delegate (IServerPlayer player, int _, CmdArgs args)
            {
                Utils.LoadWpGroups(serverAPI).ForEach(g =>
                {
                    if (!WpGroup.AllGroups.Any(og => g.GroupName == og.GroupName))
                    {
                        WpGroup.AllGroups.Add(g);
                        player.SendMessage(GlobalConstants.GeneralChatGroup, $"Added group {g}.", EnumChatType.CommandSuccess);
                    }
                });
                var count = 0;
                Utils.GetAllWaypoints(serverAPI).ForEach(w =>
                {
                    if (w.Text != null)
                    {
                        var group = WpGroup.AllGroups.FirstOrDefault(g => g.GroupName == w.Text);
                        if (group != null)
                        {
                            w.Text = null;
                            if (!group.Waypoints.Any(wp => Utils.PositionEqual(wp.Position, w.Position)))
                            {
                                count++;
                                group.Waypoints.Add(w);
                            }
                        }
                    }
                });
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Moved {count} waypoints.", EnumChatType.CommandSuccess);
            };
        }

        public static ServerChatCommandDelegate DebugFixAllGroups()
        {
            return delegate (IServerPlayer player, int _, CmdArgs args)
            {
                var wpGroups = WpGroup.AllGroups;
                wpGroups.ForEach(g =>
                { // TODO fix everything else that could be wrong in the JSON?
                    if (g.SubscribedPlayerIds == null)
                        g.SubscribedPlayerIds = new List<string>();
                    var newSubs = new List<string>();
                    g.SubscribedPlayerIds.ForEach(p =>
                    {
                        if (!newSubs.Contains(p))
                            newSubs.Add(p);
                    });
                    g.SubscribedPlayerIds = newSubs;
                });
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Fixed {wpGroups.Count} groups.", EnumChatType.CommandSuccess);
            };
        }

        public static void PurgeSubscription(ICoreServerAPI serverAPI, IServerPlayer player, CmdArgs args)
        {
            var argGroup = args.PopWord();
            var groups = WpGroup.AllGroups;
            var group = groups.SingleOrDefault(g => g.GroupName == argGroup);
            if (group == null)
            {
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Syntax: /wpg purgesub groupname", EnumChatType.CommandError);
                return;
            }
            if (!group.SubscribedPlayerIds.Remove(player.PlayerUID))
            {
                player.SendMessage(GlobalConstants.GeneralChatGroup, $"Not subscribed to group {argGroup}", EnumChatType.CommandError);
                return;
            }

            player.SendMessage(GlobalConstants.GeneralChatGroup, $"Unsubscribed from group {argGroup}", EnumChatType.CommandSuccess);
            var waypoints = Utils.GetAllWaypoints(serverAPI);
            var totalCount = waypoints.Count();
            var remaining = waypoints.Where(w => player.PlayerUID != w.OwningPlayerUid
                                              || !group.Waypoints.Any(wp => Utils.PositionEqual(wp.Position, w.Position))).ToList();
            var leftCount = remaining.Count;
            waypoints.Clear();
            waypoints.AddRange(remaining);
            player.SendMessage(GlobalConstants.GeneralChatGroup, $"Removed {totalCount - leftCount} Waypoints.", EnumChatType.CommandSuccess);
        }

        public static void ListWpGroups(IServerPlayer player)
        {
            var count = 0;
            WpGroup.AllGroups.ForEach(g =>
            {
                // null-coallecing to resolve old JSON
                bool isSubscribed = (g.SubscribedPlayerIds ?? new List<string>()).Contains(player.PlayerUID);
                player.SendMessage(GlobalConstants.GeneralChatGroup,
                                   $"{count++} {g.GroupName} {(isSubscribed ? "[subscribed]" : string.Empty)}",
                                   EnumChatType.CommandSuccess);
            });
            player.SendMessage(GlobalConstants.GeneralChatGroup, $"Found {count} groups.", EnumChatType.CommandSuccess);

        }

        public static void AddWpGroup(IServerPlayer player, CmdArgs args)
        {

            var wpgroups = WpGroup.AllGroups;
            var defGroup = WpGroup.GetDefaultGroup();
            var argGroup = args.PopWord(defGroup.GroupName);
            var argName = args.PopWord(defGroup.WpName);
            var argIcon = args.PopWord(defGroup.Icon);
            var argColor = args.PopWord(defGroup.Color);
            var newWpGroup = new WpGroup(
                groupName: argGroup,
                _wpname: argName,
                icon: WpGroup.Icons.Any((i) => argIcon == i) ? argIcon : defGroup.Icon,
                color: argColor,
                count: 0,
                subscribedPlayerIds: new List<string>(),
                waypoints: new List<Waypoint>()
            );
            if (wpgroups.FirstOrDefault(g => g.GroupName == newWpGroup.GroupName) == null)
                wpgroups.Add(newWpGroup);
            player.SendMessage(GlobalConstants.GeneralChatGroup, $"Added {newWpGroup}", EnumChatType.CommandSuccess);
        }
    }
}
