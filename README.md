# Waypointsync for Vintage Story

This mod allows for grouping and sharing of waypoints in multiplayer Vintage Story. The mod (currently) runs entirely serverside and GUIless, and exposes server commands that modify the waypoints lists. Groups can be published and are then available for other players to pull.
It is possible to subscribe to groups and automatically recieve new waypoints when they are pushed to the group. Alternatively changes can be manually pulled 

# Special thanks

Special thanks to [Github:copygirl](https://github.com/copygirl) for the mod blueprint used to setup the dev environment and this mod. You can find it at https://github.com/copygirl/howto-example-mod. The project setup instructions below are copied and adapted from that repository.


## Prerequisites

- Get the [game][VS].
- Install the [.NET Core SDK][dotnet-dl].
- Install [Visual Studio Code][vscode-dl].
- Install the [C# extension][cs-ext] from the "Extensions" tab in VS Code.
- If you run on Mono, install the [Mono Debug][mono-ext] extension.

### Extract the Game to the bin folder

the .csproj and launch.json in this project expect the game archive to be unpacked to the bin folder, such that the game exe is a at bin/vintagestory/Vintagestory.exe


## Project Setup

Now that we're done with that, it's time to set up the project structure. Create and open a new folder in VS Code!

OmniSharp gets a bit confused with Mono so if you see the following error, open your user or workspace settings (press `F1` and search for "user / workspace settings") and set "Omnisharp: Use Global Mono" to "always".

> The reference assemblies for .NETFramework,Version=v4.5.2 were not found. To resolve this, install the Developer Pack (SDK/Targeting Pack) for this framework version or retarget your application.

### Directory Structure

- `./`: The root folder is mainly where our `.csproj` file will reside, but other files are going to end up here, such as `.gitignore` if you're using Git, the readme, license, ...
- `./src/`: Contains all source (`.cs`) files for our mod. This is what makes this a code mod over a simple content mod, which just contains assets. More on this destinction on the [official wiki][VS-wiki].
- `./resources/`: This directory contains all files which will eventually be included in the release `.zip` file such as the `modinfo.json`.
- `./resources/assets/`: Contains all assets for our mod. Unlike some other games (*cough* Minecraft *cough*), a lot of the heavy lifting will already be done by the asset loading portion of the engine.



[VS]: https://www.vintagestory.at/
[VS-wiki]: http://wiki.vintagestory.at/
[VS-AUR]: https://aur.archlinux.org/packages/vintagestory
[dotnet]: https://dotnet.microsoft.com/
[dotnet-dl]: https://dotnet.microsoft.com/download
[vscode]: https://code.visualstudio.com/
[vscode-dl]: https://code.visualstudio.com/Download
[cs-ext]: https://marketplace.visualstudio.com/items?itemName=ms-vscode.csharp
[mono-ext]: https://marketplace.visualstudio.com/items?itemName=ms-vscode.mono-debug
[AUR]: https://wiki.archlinux.org/index.php/Arch_User_Repository
[carrycapacity-csproj]: https://github.com/copygirl/CarryCapacity/blob/master/CarryCapacity.csproj
[VSMods-github]: https://github.com/copygirl/VintageStoryMods
